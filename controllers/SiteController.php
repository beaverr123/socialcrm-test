<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\TestForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionTest()
    {

        $validated = null;
        $model = new TestForm();
        if(Yii::$app->request->post()){
            $model->load(Yii::$app->request->post());
            $validated = $model->validate() ? true : false;

        }

        return $this->render('test', [
            'model' => $model,
            'validated' => $validated,
        ]);
    }
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionUnique()
    {
        $textArray = [];

        if($post = Yii::$app->request->post()){
            Yii::$app->uniquetextcomponent->uniqueText($post['text'], $post['count']); // generates unique texts
            $textArray = Yii::$app->uniquetextcomponent->getAllUniqueTexts();
            //print_r(Yii::$app->uniquetextcomponent->getUniqueText()); // returns first text variant
            //print_r(Yii::$app->uniquetextcomponent->getUniqueText()); // returns second text variant
        }

        return $this->render('unique', [
            'textArray' => $textArray
        ]);
    }
}
