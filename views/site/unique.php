<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Unique text';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <form method="post" action="<?=Url::to(['/site/unique'], true)?>">
        <label for="text">текст для унификации</label>
        <input type="text" name="text">
        <label for="text">количество вариантов</label>
        <input type="number" name="count">
        <input type="submit" value="Сгенерировать">
        <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
    </form>

    <?php if(count($textArray) >0): ?>
    <?php foreach($textArray as $key=>$text): ?>
        <p><?=$key?> => <?= $text ?></p>
    <?php endforeach; ?>
    <?php endif; ?>
</div>
