<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class TestForm extends Model
{
    public $text;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['text', 'validateTextBrackets'],
        ];
    }

    /**
     * Validates the text.
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateTextBrackets($attribute, $params)
    {
        $opened = [];
        $hasErrors = false;

        if (!$this->hasErrors()) {
            foreach(str_split ($this->text) as $char){
                if($char == '[' || $char == '(')
                    $opened[] = $char;
                elseif($char == ']'){
                    if( array_pop($opened) !='[') $hasErrors = true;
                }elseif($char == ')'){
                    if( array_pop($opened) !='(') $hasErrors = true;
                }
            }
            if(count($opened) > 0) $hasErrors = true;

            if($hasErrors) $this->addError($attribute, 'Incorrect brackets sequence');
        }
    }

}
