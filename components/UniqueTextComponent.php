<?php

namespace app\components;


use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class UniqueTextComponent extends Component
{
    private $pattern = [
        'о'=>'1',
        'с'=>'2',
        'а'=>'3',
        'е'=>'4',
        'р'=>'5',
        'у'=>'6',
        'х'=>'7'
    ];
    private $variantsOffset = 0;
    private $replacementPositions = [];
    private $uniqueTexts = [];


    public function setReplacementPattern(array $pattern)
    {
        $this->pattern = $pattern;
    }
    public function setVariantsOffset(int $offset)
    {
        $this->variantsOffset = $offset;
    }

    public function uniqueText($text, $numbers)
    {
        $this->analyzeTextForReplacements($text);
        $replacePosArray = $this->getReplacePositions($numbers, $this->variantsOffset);
        $this->uniqueTexts = $this->generateUniqueTexts($replacePosArray,$text);

    }

    public function getAllUniqueTexts()
    {
        return $this->uniqueTexts;
    }

    public function getUniqueText()
    {
        $text = current($this->uniqueTexts);
        next($this->uniqueTexts);

        return $text;
    }

    private function analyzeTextForReplacements($text)
    {
        foreach($this->pattern as $char=>$val){
            $offset = 0;
            do{
                $pos = mb_strpos($text, $char,$offset);
                if($pos !== false) {
                    $this->replacementPositions[] = [$char =>$pos];
                    $offset = $pos+1;
                }
            }while($pos !== false);
        }
    }

    private function getReplacePositions($n, $offsetResults = 0)
    {
        $resultReplaceArray = [];
        $start = 0;
        $j = 0;
        $startArr = [];
        for($i=0;$i<=$n + $offsetResults -1;$i++){
            $repArr = [];
            if(!isset($this->replacementPositions[$start+$j])){
                $startArr[] = $this->replacementPositions[$start];
                $start++;
                $j=0;
            }
            if(count($startArr) > 0){
                $repArr = $startArr;
            }
            if(!isset($this->replacementPositions[$start+$j])) break;
            $repArr[] = $this->replacementPositions[$start+$j];
            if($i >= $offsetResults){
                $resultReplaceArray[] = $repArr;
            }
            $j++;
        }
        return $resultReplaceArray;
    }

    private function generateUniqueTexts($replacePosArray,$text)
    {
        $uniqueTexts = [];

        foreach($replacePosArray as $replaceArr){
            $modifiedText = $text;
            foreach($replaceArr as $replacePosition){
                $char = key($replacePosition);
                $replacement = $this->pattern[$char];
                //print_r("меняем $char на $replacement в позиции $replacePosition[$char] \n"); // ,, $replacePosition
                $modifiedText = $this->mb_substr_replace($modifiedText,$replacement,$replacePosition[$char],1);
            }
            $uniqueTexts[bin2hex(random_bytes(20))] = $modifiedText;
        }

        return $uniqueTexts;
    }

     /**
     * @param mixed $string The input string.
     * @param mixed $replacement The replacement string.
     * @param mixed $start If start is positive, the replacing will begin at the start'th offset into string.  If start is negative, the replacing will begin at the start'th character from the end of string.
     * @param mixed $length If given and is positive, it represents the length of the portion of string which is to be replaced. If it is negative, it represents the number of characters from the end of string at which to stop replacing. If it is not given, then it will default to strlen( string ); i.e. end the replacing at the end of string. Of course, if length is zero then this function will have the effect of inserting replacement into string at the given start offset.
     * @return string The result string is returned. If string is an array then array is returned.
     */
    private function mb_substr_replace($string, $replacement, $start, $length=NULL) {
    if (is_array($string)) {
        $num = count($string);
        // $replacement
        $replacement = is_array($replacement) ? array_slice($replacement, 0, $num) : array_pad(array($replacement), $num, $replacement);
        // $start
        if (is_array($start)) {
            $start = array_slice($start, 0, $num);
            foreach ($start as $key => $value)
                $start[$key] = is_int($value) ? $value : 0;
        }
        else {
            $start = array_pad(array($start), $num, $start);
        }
        // $length
        if (!isset($length)) {
            $length = array_fill(0, $num, 0);
        }
        elseif (is_array($length)) {
            $length = array_slice($length, 0, $num);
            foreach ($length as $key => $value)
                $length[$key] = isset($value) ? (is_int($value) ? $value : $num) : 0;
        }
        else {
            $length = array_pad(array($length), $num, $length);
        }
        // Recursive call
        return array_map(__FUNCTION__, $string, $replacement, $start, $length);
        }
        preg_match_all('/./us', (string)$string, $smatches);
        preg_match_all('/./us', (string)$replacement, $rmatches);
        if ($length === NULL) $length = mb_strlen($string);
        array_splice($smatches[0], $start, $length, $rmatches[0]);
        return join($smatches[0]);
    }

}